package com.aris.plugins.my_pid;

import android.os.Process;
import android.util.Log;

public class MyPid {

    public String echo(String value) {
        Log.i("Echo", value);
        return value;
    }

    public int getPid() {
        return Process.myPid();
    }

    public void killMyPid() {
        Process.killProcess(Process.myPid());
    }
}
