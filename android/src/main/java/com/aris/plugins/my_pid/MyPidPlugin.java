package com.aris.plugins.my_pid;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "MyPid")
public class MyPidPlugin extends Plugin {

    private MyPid implementation = new MyPid();

    @PluginMethod
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", implementation.echo(value));
        call.resolve(ret);
    }

    @PluginMethod
    public void getPid(PluginCall call) {
        JSObject ret = new JSObject();
        ret.put("value", implementation.getPid());
        call.resolve(ret);
    }

    @PluginMethod
    public void killMyPid(PluginCall call) {
        implementation.killMyPid();
        call.resolve();
    }
}
