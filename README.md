# my-pid

Capacitor plugin to fetch current PID in android

## Install

```bash
npm install my-pid
npx cap sync
```

## API

<docgen-index>

* [`echo(...)`](#echo)
* [`getPid()`](#getpid)
* [`killMyPid()`](#killmypid)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### echo(...)

```typescript
echo(options: { value: string; }) => Promise<{ value: string; }>
```

| Param         | Type                            |
| ------------- | ------------------------------- |
| **`options`** | <code>{ value: string; }</code> |

**Returns:** <code>Promise&lt;{ value: string; }&gt;</code>

--------------------


### getPid()

```typescript
getPid() => Promise<number>
```

**Returns:** <code>Promise&lt;number&gt;</code>

--------------------


### killMyPid()

```typescript
killMyPid() => Promise<void>
```

--------------------

</docgen-api>
