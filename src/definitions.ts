export interface MyPidPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  getPid(): Promise<number>;
  killMyPid(): Promise<void>;
}
