import { registerPlugin } from '@capacitor/core';

import type { MyPidPlugin } from './definitions';

const MyPid = registerPlugin<MyPidPlugin>('MyPid', {
  web: () => import('./web').then(m => new m.MyPidWeb()),
});

export * from './definitions';
export { MyPid };
