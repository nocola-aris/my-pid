import { WebPlugin } from '@capacitor/core';

import type { MyPidPlugin } from './definitions';

export class MyPidWeb extends WebPlugin implements MyPidPlugin {
  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async getPid(): Promise<number> {
    throw this.unavailable('Get PID is available in android only.');
  }

  async killMyPid(): Promise<void> {
    throw this.unavailable('Kill my PID is available in android only.');
  }
}
