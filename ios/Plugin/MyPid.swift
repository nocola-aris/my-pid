import Foundation

@objc public class MyPid: NSObject {
    @objc public func echo(_ value: String) -> String {
        print(value)
        return value
    }
}
